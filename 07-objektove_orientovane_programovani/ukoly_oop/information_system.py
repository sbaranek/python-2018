'''
V tomto cviceni si vytvorime jednoduchy system pro ukladani informaci o lidech vcetne jejich polohy.

1. Vytvorte tridu Person, ktera bude mit tyto vlastnosti:
    - id (nahodne generovane cislo)
    - name (str)
    - surname (str)
    - birthdate (str) ve formátu DD.MM.RRRR
    - age (int) - vypocitany z data narozeni
    - location (Location) ve formatu latitude longitude
pokud uzivatel zada hodnotu se spatnym datovym typem, tak vyhodite vyjimku

2. Vytvorte tridu Location, ktera bude obsahovat nasledujici parametry
    - latitude (float)
    - longitude (float)
    - wkt (str) - bude obsahovat geometrii prevedenou na wkt (wkt - https://en.wikipedia.org/wiki/Well-known_text)
    - city

3. ziskejte od uzivatelu hodnoty pomoci input() a vytvorte jednotlive objekty
4. implementujte funkce na vypis informaci o person i location objektech
5. vytvorte funkci, ktera vypocita vek daneho cloveka podle data narozeni
6. vytvorte funkci, ktera vytvori wkt bod z latitude a longitude
7. napiste funkci na zmenu prijmeni urcite osoby
8. zkuste vymyslet zpusob jak zavolat OpenStreetMap API pro ziskani mesta, kde clovek zije
    https://nominatim.openstreetmap.org/

'''
import random
from datetime import datetime
from geopy.geocoders import Nominatim


class Location:

    def __init__(self, lat, lon):
        if not (isinstance(lat, float) or isinstance(lon, float)):
            raise ValueError('One of the provided values has wrong data type')

        self.lat = lat
        self.lon = lon

        self.wkt = 'POINT({} {})'.format(lat, lon)
        self.city = self.get_city()

    def get_city(self):
        geolocator = Nominatim(user_agent="myapp")
        location = geolocator.reverse("{}, {}".format(self.lat, self.lon))
        return location.address

    def __str__(self):
        return 'Latitude: {}, Longitude: {}, WKT: {}, City: {}'.format(self.lat, self.lon, self.wkt, self.city)

    def __repr__(self):
        return self.__str__()


class Person:

    def __init__(self, name, surname, birthdate, lat, lon):
        self.id = random.randint(0,1000)

        try:
            self.name = name
            self.surname = surname
            self.birthdate = birthdate
        except ValueError:
            print('One of the provided values has wrong data type')

        self.age = self.get_age()
        self.location = Location(lat, lon)

    def get_age(self):
        bdate = datetime.strptime(self.birthdate, '%d.%m.%Y')
        return (datetime.today() - bdate).days/365

    def change_surname(self, new_surname):
        self.surname = new_surname

    def __str__(self):
        return 'ID: {}, Name: {}, Surname: {}, Birthdate: {}, Age: {}, Location: {}'.format(
            self.id, self.name, self.surname, self.birthdate, self.age, self.location
        )

    def __repr__(self):
        return self.__str__()


persons = []
new_person = 'Y'

while new_person == 'Y':
    # Petr,Silhak,05.07.1993,49.20235,16.60673
    name, surname, birthdate, latitude, longitude = input(
        """Zadejte informace o uzivateli ve formatu: jmeno,prijmeni,datumnarozeni(DD.MM.RRRR),zem. sirka,zem. delka: \n""").split(',')
    persons.append(Person(name, surname, birthdate, float(latitude), float(longitude)))
    new_person = input('Chcete zadat dalsi osobu(Y/N)?\n')

print(persons)

